#ifndef CLASS_BLOCK
#define CLASS_BLOCK

#include <vector>
#include "Pixel.h"

using namespace std;

class Block {
	protected:
		vector<vector<Pixel> > pixeles;
	public:
		Block(vector<vector<Pixel> > pixeles){
			this->pixeles = pixeles;
		}
		~Block(){ pixeles.clear(); }
		void showPixeles(){
			cout << endl;
			for(int i = 0; i<this->pixeles.size();i++){
				for(int j=0; j<this->pixeles[i].size();j++){
					cout << endl;
					this->pixeles[i][j].showCanales();
				}
			}
		}


};

#endif