#ifndef CLASS_IMAGE
#define CLASS_IMAGE

#include <vector>
#include "Block.h"
#include "Pixel.h"

using namespace std;
using namespace cv;

class Image {
	private: 
		vector<vector<Block> > bloques;
	public: 
		Image(Mat image){
			vector<vector<Pixel> > pixeles;
			vector<Pixel> pixels;
			vector<Block> bloque;
			vector<float> canales;
			
			int cantidad_canales = 1;
			if(image.type() == CV_8UC3){
				cantidad_canales = 3;
			}

			for(int i=0; i<image.rows/2; i++){
				for(int j=0; j<image.cols/2; j++){

					for(int k=0; k<2; k++){
						for(int l=0; l<2; l++){
							for(int m=0; m<cantidad_canales; m++){
								canales.push_back((float)image.at<Vec3b>(k+(i*2),l+(j*2))[m]);
							}
							pixels.push_back(canales);		
							canales.clear();
						}
						pixeles.push_back(pixels);
						pixels.clear();
					}
					bloque.push_back(pixeles);
					pixeles.clear();
				}
				this->bloques.push_back(bloque);
				bloque.clear();
			}

			for(int i = 0; i<this->bloques.size();i++){
				for(int j=0; j<this->bloques[i].size();j++){
					this->bloques[i][j].showPixeles();
				}
			}
		}
		~Image(){ bloques.clear(); }

};

#endif