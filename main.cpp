#include <iostream>
#include <opencv2/opencv.hpp>
// #include "Pixel.h"
// #include "Block.h"
#include "Image.h"
#define TAM_BLOCK 2

using namespace std;
using namespace cv;

void show(Mat image){
    int LIM = 1;
    if(image.type() == CV_8UC3){
        LIM = 3;
    }

    for(int i = 0; i < image.rows; i++){
        for(int j = 0; j < image.cols; j++){
            cout << "( ";
            for(int k=0; k<LIM; k++){
                cout<<(int)image.at<Vec3b>(i,j)[k]<<" ";
            }
            cout<<")\t";
        }
        cout << endl;
    }
}

int main(int argc, char** argv ){
    string archivo;
    Mat image;

    if( argc > 1)
    {
        archivo = argv[1];
    }
    if( argc > 2){
        if(strcmp(argv[2], "--color") == 0){
            cout << "Leyendo imagen color" << endl;
            image = imread(archivo.c_str(), IMREAD_COLOR); //Leer archivo imagen
        }
    }else{
        cout << "Leyendo imagen escala de grises" << endl;
        image = imread(archivo.c_str(), IMREAD_GRAYSCALE); //Leer archivo imagen
    }

    if( image.empty() ){ // Revisión para entrada invalida
        cout <<  "No se pudo abrir o encontrar la imagen" << endl ;
        return -1;
    }

    if( image.rows != image.cols){ // Revisión para imagenes no cuadradas
        cout << "La imagen seleccionada no es cuadrada (n*n)" << endl;
        return -1;
    }

    show(image);
    Image imagen(image);
    return 0;
}