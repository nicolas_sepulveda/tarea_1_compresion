#ifndef CLASS_PIXEL
#define CLASS_PIXEL

#include <vector>

using namespace std;

class Pixel {
	protected:
		vector<float> canales;
	public:
		Pixel(vector<float> canales){
			this->canales = canales; 
		}
		~Pixel(){ canales.clear(); }
		void showCanales(){
			for(int i=0; i < canales.size(); i++){
				cout << this->canales[i]<< " ";
			}
		}
};

#endif