# Tarea 1 Electivo Compresión
## Comandos de compilación
``` 
g++ $(pkg-config --cflags --libs opencv) main.cpp -o main 
```
## Comandos de ejecución
Imagen escala de grises
``` 
./main black_dreg.bmp 
```
Imagen a color (RGB)
``` 
./main red_dreg.bmp --color
```